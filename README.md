# Flyr

> <b>f</b>etch <b>lyr</b>ics

Flyr is a small utility used to download lyrics from common
website.

## Usage

```sh
flyr [-hnp] [-f config] [-o out] artist song_title
```

 - `-h` help
 - `-n` configtest mode
 - `-p` print the config
 - `-f` specify another config file
 - `-o` save the lyrics to the given file instead of `stdout`
 
Please refer to the [manpage](flyr.1.md) for a more specific
description of the flags.

## Config file

The syntax is described in the relative manpage
[flyrrc.5](flyrrc.5.md). In most cases you can simply copy the
[`flyrrc`](flyrrc) file that comes with the code and use that

```sh
$ cp flyrrc ~/.flyrrc
```

## Dependencies

 - `libcurl`
 - `libtidy`: should come with the `tidy` or `tidyp` package
 - `utf8.h`: bundled here, so not an external deps
 - `libbsd`: on some system.
 
for the target `manpages` of the Makefile `mandoc` is required, but a
pre-markdown-izen version of the manpage is bundled with the code.

## Build

Please, read the [`Makefile`](Makefile) and setup the variables accordly to your
system. The makefile is widely commentend, so it should be easy. Then
execute

```sh
$ make
```

## Portability

This should run without issue on every system supported by `libcurl`
and `libtidy` with a reasonable recent C compiler and a libc with
`asprintf(3)` (that is present on various
[BSDs](https://man.openbsd.org/asprintf), in [GNU
libc](https://sourceware.org/git/?p=glibc.git;a=blob;f=stdio-common/asprintf.c;hb=d6db68e66dff25d12c3bc5641b60cbd7fb6ab44f)
and
[musl](http://git.musl-libc.org/cgit/musl/tree/src/stdio/asprintf.c),
so I think it could be thought as *portable*?).

In addition to that, this program use
[`strlcat(3)`](https://man.openbsd.org/strlcat), so you need `libbsd`
or some kind of replacement for that as well.

## Acknowledgement

 - `utf8.h` is a small library for utf8 handling released into the
   public domain
