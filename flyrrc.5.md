FLYRRC(5) - File Formats Manual

# NAME

**flyrrc** - Configuration file for the fetch lyrics utility

# DESCRIPTION

**flyrrc**
is the configuration file for the
flyr(1)
utility.

Comments line **must** start with an hash sign '#' and are
completely ignored. Empty line are also ignored. Every other define a
*rule*.

A rule is made by three field divided by a tab character. The first
fild is the name of the site, it's used only when
flyr(1)
dumps the config.

The second field is the url template of the lyrics. When querying for
a lyric
flyr(1)
will first replace the occurrence of %artist% and %title% with a
lowercase, snake-case version of the given artist and title
respectively and then will perform a HTTP GET request to that url.

The third is a CSS-like selector to select the text. If a HTML element
matches that selector, all the text (recursively) inside that will be
outputted. As of now
flyr(1)
supports only a limited subset of the CSS selectors.

# CSS SELECTORS

The syntax for the selector supported is a list of simple
selector. Each simple selector is in the form

	tag#id.class

where only one of tag, id and class must be specified at least.

Each of this simple selector can be combined with other in a list to
create a more complex selector. A nested selector matches an element
only if, upper in the HTML tree, all the predecessor of that selector
matched. To clarify this, let's say that the target page has a HTML
structure like the following

	<div id="some-div">
	    <!-- ... -->
	</div>
	
	<div class="some-other-div">
	    <p class="lyrics">
		<!-- lyrics here -->
	    </p>
	</div>

to select the lyrics we can use "div.some-other-div .lyrics", while
"#some-div .lyrics" does not match what we want.

# EXAMPLES

An example (and working) configuration

	# Sites:
	Genius	https://genius.com/%artist%-%title%-lyrics	div.song_body-lyrics div.lyrics p
	Metro	http://www.metrolyrics.com/%title%-lyrics-%artist%.html	p.verse

OpenBSD 6.3 - August 19, 2018
