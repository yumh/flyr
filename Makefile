VERSION	 = 0.1

# The good news is that if you're runnig OpenBSD chance are that you
# don't need to change anything! Yay!
# The bad news is that, otherwise, you have to change something in
# order to build, but I'm here to help you!

# The comment are the lines starting with a '#' and then a space, the
# commented code starts with '#' and doesn't have spaces after.

# Most systems have a package/port called `tidy' in their
# repositories. If you have that uncomment the following line
#TIDY	 = TIDY
# and comment this
TIDY	 = TIDYP
# If you're system, like OpenBSD, has only tidyp leave this paragraph
# untouched.

# If you have tidy you may also want to uncomment these lines to setup
# the CFLAGS and LIBS correctly
#CFLAGS	+= `pkg-config --libs tidy`
#LIBS	+= `pkg-config --libs tidy`
# and comment this line
LIBS	+= -ltidyp

# If you're on a GNU system you have to link against libbsd and define
# the macro _GNU_SOURCE (to "enable" asprintf(3))
#CFLAGS	+= `pkg-config --cflags libbsd-overlay`
#LIBS	+= `pkg-config --libs libbsd-overlay`
#CFLAGS	+= -D_GNU_SOURCE

# This applies only to OpenBSD. Comment if you're building on other
# systems.
CDEFS	+= -DHAVE_PLEDGE

# So, it wasn't that hard, was it? What follows is some boring stuff
# that most of the time you don't need to update.

# You may not want to change these
CC	?= cc
OPTIM	?= -O3
CDEFS	+= -D${TIDY}
CFLAGS	+= ${OPTIM} ${CDEFS} `pkg-config --cflags libcurl`
LIBS	+= `pkg-config --libs libcurl`

.PHONY: all clean manpages debug

all: flyr

debug:
	make OPTIM="-O0 -g -Wall"

flyr: flyr.c
	${CC} ${CFLAGS} flyr.c -o flyr ${LIBS}

clean:
	rm -f flyr

manpages: flyr.1.md flyrrc.5.md

flyr.1.md: flyr.1
	mandoc -T markdown flyr.1 > flyr.1.md

flyrrc.5.md: flyrrc.5
	mandoc -T markdown flyrrc.5 > flyrrc.5.md
