#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <sysexits.h>
#include <string.h>

#include <curl/curl.h>

#ifndef TIDY
# include <tidyp/tidyp.h>
# include <tidyp/buffio.h>
#else
# include <tidy.h>
# include <tidybuffio.h>
#endif

#include "utf8.h"

#define nil NULL

#define INITIAL_SOURCES 16

#define INITIAL_BUFLEN 1024

#define MIN_LYRIC_LEN 10

struct selector {
	char *tag;
	char *class;
	char *id;
	struct selector *next;
};

struct source {
	char *name;
	char *url_template;
	struct selector *css_selector;
};

struct config {
	struct source *sources;
	size_t len;
	size_t capacity;
};

struct selector *
parse_selector(const char *s)
{
	struct selector root = {
		.tag = nil,
		.id = nil,
		.class = nil,
	};

	struct selector *r = &root;

	char *selector = strdup(s);
	if (selector == nil)
		return nil;

	char *tok = selector;
	char *end = selector;
	while (tok != nil) {
		strsep(&end, " ");
		if (strlen(tok)) { /* skip empty */
			r->next = calloc(1, sizeof(struct selector));
			if (r->next == nil)
				break;
			r = r->next;

			char *tend  = strchr(tok, '\0');
			char *pound = strchr(tok, '#');
			char *dot   = strchr(tok, '.');

			if (dot == nil) {
				dot = tend;
			} else {
				*dot = 0;
				dot++;
			}

			if (pound == nil) {
				if (strchr(tok, '.') == nil)
					pound = tend;
				else
					pound = dot-1;
			} else {
				*pound = 0;
				pound++;
			}

			if (strlen(tok))
				r->tag   = strdup(tok);
			if (strlen(pound))
				r->id    = strdup(pound);
			if (strlen(dot))
				r->class = strdup(dot);
		}
		tok = end;
	}

	free(selector);
	return root.next;
}

void
destroy_selector(struct selector *s)
{
	while (s != nil) {
		free(s->tag);
		free(s->id);
		free(s->class);
		struct selector *next = s->next;
		free(s);
		s = next;
	}
}

struct source
parse_line(char *l)
{
	struct source s = {
		.name = nil,
		.url_template = nil,
		.css_selector = nil
	};

	char *tok = l;
	char *end = l;
	int i = 0;
	while (tok != nil) {
		strsep(&end, "\t");

		if (i == 0) {
			s.name = strdup(tok);
			if (s.name == nil)
				goto memerr;
		}

		if (i == 1) {
			s.url_template = strdup(tok);
			if (s.url_template == nil)
				goto memerr;
		}

		if (i == 2) {
			s.css_selector = parse_selector(tok);
			if (s.css_selector == nil)
				goto memerr;
		}

		if (i > 2)
			goto syntaxerr;

		tok = end;
		++i;
	}

	if (i != 3)
		goto syntaxerr;

	return s;

 syntaxerr:
	fprintf(stderr, "Syntax error!\n");
	goto exitabn;

 memerr:
	fprintf(stderr, "Memory allocation error.\n");

 exitabn:
	if (i > 0) {
		free(s.name);
		s.name = nil;
	}
	if (i > 1) {
		free(s.url_template);
		s.url_template = nil;
	}
	if (i > 2) {
		free(s.css_selector);
		s.css_selector = nil;
	}
	return s;
}

void
destroy_config(struct config *c)
{
	for (size_t i = 0; i < c->len; i++) {
		free(c->sources[i].name);
		free(c->sources[i].url_template);
		destroy_selector(c->sources[i].css_selector);
	}
	free(c->sources);
	free(c);
}

struct config *
parse_config_str(const char *config, size_t len)
{
	struct config *c = malloc(sizeof(struct config));
	if (c == nil) {
		fprintf(stderr, "Memory allocation error.\n");
		return nil;
	}
	c->len = 0;
	c->capacity = INITIAL_SOURCES;
	c->sources = calloc(c->capacity, sizeof(struct config));
	if (c->sources == nil) {
		free(c);
		fprintf(stderr, "Memory allocation error.\n");
		return nil;
	}

	size_t lines = 0;
	bool in_comment = false;
	for (size_t i = 0; i < len; ++i) {
		char ch = config[i];

		/* eat up comments */
		if (in_comment && ch == '\n') {
			in_comment = false;
			lines++;
			continue;
		}

		if (in_comment)
			continue;

		/* start comment */
		if (ch == '#') {
			in_comment = true;
			continue;
		}

		/* otherwise read the whole line */
		const char *end = strchr(config + i, '\n');
		if (end == nil) /* if there is no newline read 'till the end */
			end = config + len;

		size_t linelen = end - (config + i);

		lines++;

		if (linelen == 0)	/* skip over empty lines */
			continue;

		/* +1 for NUL terminator */
		char *l = calloc(linelen + 1, sizeof(char));
		if (l == nil) {
			fprintf(stderr, "Cannot allocate memory!\n");
			destroy_config(c);
			return nil;
		}

		memcpy(l, config + i, linelen);

		struct source s = parse_line(l);
		free(l);
		if (s.name == nil || s.url_template == nil || s.css_selector == nil) {
			fprintf(stderr, "Syntax error or memory allocation error while parsing line %ld of config file.\n", lines);
			destroy_config(c);
			return nil;
		}
		c->sources[c->len] = s;

		c->len++;
		i += linelen;

		if (c->len == c->capacity) {
			c->capacity += c->capacity>>1;
			size_t newcap = c->capacity + (c->capacity>>1);
			struct source *s = realloc(c->sources, newcap);
			if (s == nil) {
				fprintf(stderr, "Cannot allocate memory!\n");
				destroy_config(c);
				return nil;
			}
			c->sources = s;
			c->capacity = newcap;
		}
	}

	return c;
}

struct config *
parse_config(const char *path)
{
	int fd = open(path, O_RDONLY);

	if (fd == -1) {
		perror("Can't open config file");
		return nil;
	}

	/* get file size */
	off_t file_size = lseek(fd, 0, SEEK_END);
	if (file_size == -1) {
		perror("Can't seek in config file");
		close(fd);
		return nil;
	}

	lseek(fd, 0, SEEK_SET); /* rewind to the start of the file */

	/* map config file into memory */
	char *f = mmap(nil, file_size, PROT_READ, MAP_PRIVATE, fd, 0);
	close(fd);
	if (f == MAP_FAILED) {
		perror("Can't map into memory the config file");
		return nil;
	}

	struct config *c = parse_config_str(f, file_size);
	munmap(f, file_size);

	return c;
}

/* replace *first* occurence of string `patt' with `sub' in `src' */
char *
strrf(const char *src, const char *patt, const char *sub)
{
	char *c = strstr(src, patt);
	if (c == nil)
		return strdup(src);

	/* ok - patt is within src */
	size_t srclen  = strlen(src);
	size_t pattlen = strlen(patt);
	size_t sublen  = strlen(sub);
	size_t reslen  = srclen - pattlen + sublen + 1;

	char *res = calloc(1, reslen);

	memcpy(res, src, c - src);
	strlcat(res, sub, reslen);
	strlcat(res, c + pattlen, reslen);

	return res;
}

/*
 * replace EVERY occurrence of patt with sub in src. The returned
 * string have to be free'd by the callee.
 */
char *
strrepl(const char *src, const char *patt, const char *sub)
{
	if (src  == nil) return nil;
	if (patt == nil) return nil;
	if (sub  == nil) return nil;

	char *r = strdup(src);
	while (strstr(r, patt) != nil) {
		char *rr = strrf(r, patt, sub);
		free(r);
		r = rr;
	}
	return r;
}

char *
strtolower(const char *src)
{
	char *s = strdup(src);
	if (s == nil)
		return nil;
	utf8lwr(s);
	return s;
}

char *
strtosnakec(const char *c)
{
	char *f = strtolower(c);
	if (f == nil) return nil;

	char *t = f;
	for (; *t; ++t)
		if (*t == ' ')
			*t = '-';
	return f;
}

void
removestrangechar(char *c)
{
	if (c == nil)
		return;

	for (; *c; ++c) {
		if (*c == '\'' || *c == '\"' || *c == ',' || *c == '.') {
			char *t = c;
			for (; *t; ++t)
				*t = *(t+1);
		}
	}
}

char *
prepare_url(const char *template, const char *artist, const char *title)
{
	char *a = strtosnakec(artist);
	if (a == nil) return nil;
	removestrangechar(a);

	char *t = strtosnakec(title);
	if (t == nil) {
		free(a);
		return nil;
	}
	removestrangechar(t);

	char *u = strrepl(template, "%artist%", a);
	if (u == nil) {
		free(a);
		free(t);
		return nil;
	}

	char *url = strrepl(u, "%title%", t);
	if (url == nil) {
		free(a);
		free(t);
		free(u);
	}
	return url;
}

size_t
write_callback(char *in, size_t size, size_t nmemb, TidyBuffer *out)
{
	uint r = size * nmemb;
	tidyBufAppend(out, in, r);
	return r;
}

char *
buffappend(char *buf, size_t *buflen, size_t *bufw, const char *s, bool inewl)
{
	size_t len = strlen(s);
	if (*bufw + len > *buflen -1){
		size_t newlen = *buflen + (*buflen >>1);
		char *b = realloc(buf, newlen);

		if (b == nil)
			return buf;

		buf = b;
		*buflen = newlen;
		for (size_t i = *bufw; i < newlen; ++i)
			buf[i] = 0;
	}

	if (inewl) {
		for (; *s; *bufw += 1, ++s)
			if (*s == '\n') {
				*bufw -= 1;
				continue;
			}
			else
				buf[*bufw] = *s;

	} else {
		memcpy(buf + *bufw, s, len);
		*bufw += len;
	}

	return buf;
}

void
dump_text(TidyDoc doc, TidyNode tnod, char **buf, size_t *buflen, size_t *bufw)
{
	for (TidyNode child = tidyGetChild(tnod); child; child = tidyGetNext(child)) {
		ctmbstr name = tidyNodeGetName(child);
		if (!name) {
			TidyBuffer b;
			tidyBufInit(&b);
			tidyNodeGetText(doc, child, &b);

			/* printnoln(buf.bp); */
			*buf = buffappend(*buf, buflen, bufw, b.bp, true);
		} else {
			if (!strcmp("br", name))
				*buf = buffappend(*buf, buflen, bufw, "\n", false);
		}

		/* recursive */
		dump_text(doc, child, buf, buflen, bufw);

		if (name && !strcmp("p", name))
			*buf = buffappend(*buf, buflen, bufw, "\n", false);
	}
}

void
traverse(TidyDoc doc, TidyNode tnod, struct selector *selector, char **buf, size_t *buflen, size_t *bufw)
{
	if (selector == nil) {
		/* we've exhausted all the rules, get all text! */
		dump_text(doc, tnod, buf, buflen, bufw);
		return;
	}

	for (TidyNode child = tidyGetChild(tnod); child; child = tidyGetNext(child)) {
		ctmbstr name = tidyNodeGetName(child);
		if (name) {	/* if it has a name then it's an HTML tag */

			if (selector->tag == nil || !strcmp(selector->tag, name)) {
				/* tag matched */

				bool class_matched = selector->class == nil;
				bool id_matched    = selector->id    == nil;
				for (TidyAttr attr = tidyAttrFirst(child); attr; attr = tidyAttrNext(attr)) {
					if (!strcmp(tidyAttrName(attr), "class")) {
						if (strstr(tidyAttrValue(attr), selector->class) != nil) {
							class_matched = true;
						}
					}

					if (!strcmp(tidyAttrName(attr), "id")) {
						if (!strcmp(tidyAttrValue(attr), selector->id)) {
							id_matched = true;
						}
					}
				}

				if (class_matched && id_matched)
					traverse(doc, child, selector->next, buf, buflen, bufw);
			}
		}
		traverse(doc, child, selector, buf, buflen, bufw);
	}
}

/*
 * some (basic) heuristic to understand if the text retriven can be a
 * lyrics or not. Some site do not correctly use the HTTP code, they
 * return 200 even when they should really use 404. This should
 * mitigate 'em a bit. Any suggestion regard how to improve this
 * heuristic is welcomed.
 *
 * `buflen' is the maximum size of the buffer, not the actual content
 * lenght.
 */
bool
heuristic(char *buf, size_t buflen)
{
	/* simplest case, empty res */
	if (buf == nil || *buf == '\0')
		return false;

	/* res is smaller than MIN_LYRIC_LEN */
	for (size_t i = 0; i < buflen && i < MIN_LYRIC_LEN; ++i)
		if (!buf[i])
			return false;

	return true;
}

bool
fetch_lyrics(struct config *c, int out, const char *artist, const char *title)
{
	bool found = false;
	for (size_t i = 0; i < c->len && !found; ++i) {
		char *url = prepare_url(c->sources[i].url_template, artist, title);
		if (url == nil)
			goto ret;

		CURL *curl = curl_easy_init();
		if (!curl) {
			free(url);
			goto ret;
		}

		size_t buflen = INITIAL_BUFLEN;
		size_t bufw = 0;
		char *buf = calloc(buflen, sizeof(char));
		if (buf == nil) {
			free(url);
			curl_easy_cleanup(curl);
			goto ret;
		}

		TidyBuffer docbuf = { .bp = nil };
		TidyBuffer tidy_errbuf = { .bp = nil };
		char curl_errbuf[CURL_ERROR_SIZE] = {0};

		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, curl_errbuf);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_callback);

		TidyDoc tdoc = tidyCreate();
		tidyOptSetBool(tdoc, TidyForceOutput, true); /* try harder */
		tidyOptSetBool(tdoc, TidyHideComments, true); /* hide HTML comments */
		tidyOptSetInt(tdoc, TidyWrapLen, 4096);
		tidySetErrorBuffer(tdoc, &tidy_errbuf);
		tidyBufInit(&docbuf);

		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &docbuf);

		CURLcode res = curl_easy_perform(curl);

		free(url);

		/* printf("HTTP code: %d\n", res); */
		if (res != CURLE_OK) {
			fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
		} else {
			int err = tidyParseBuffer(tdoc, &docbuf);
			if (err >= 0) {
				err = tidyCleanAndRepair(tdoc); /* fix any problems */
				if (err >= 0) {
					err = tidyRunDiagnostics(tdoc);
					if (err >= 0) {
						traverse(tdoc, tidyGetRoot(tdoc), c->sources[i].css_selector, &buf, &buflen, &bufw);

						if (heuristic(buf, buflen)) {
							found = true;
							write(out, buf, bufw);
							write(out, "\n", 1);
						}
						/* fprintf(stderr, "error buffer: %s\n", tidy_errbuf.bp); */
					}
				}
			}
		}

		curl_easy_cleanup(curl);
		tidyBufFree(&docbuf);
		tidyBufFree(&tidy_errbuf);
		tidyRelease(tdoc);
	}

 ret:
	return found;
}

void
usage(char *prgname)
{
	fprintf(stderr, "Usage: %s [-hnp] [-f config] [-o out] artist song_title\n", prgname);
}

int
main(int argc, char **argv)
{
#ifdef HAVE_PLEDGE
	/*
	 * stdio: basic I/O & such
	 * rpath wpath cpath: read, write & create file. Later some of these will be dropped
	 * inet dns: perform GET request
	 */
	pledge("stdio rpath wpath cpath chown inet dns", "");
#endif

	char *prgname = *argv;

	char *config_file;
	asprintf(&config_file, "%s/.flyrrc", getenv("HOME"));

	bool check_only   = false;
	bool print_config = false;

	int out = 1;

	int ch;
	while ((ch = getopt(argc, argv, "hnpf:o:")) != -1) {
		switch(ch) {
		case 'h':
			usage(prgname);
			return 0;

		case 'n':
			check_only = true;
			break;

		case 'p':
			print_config = true;
			break;

		case 'f':
			free(config_file);
			config_file = strdup(optarg);
			break;

		case 'o':
			out = open(optarg, O_WRONLY | O_CREAT, S_IRWXU);
			if (out == -1) {
				free(config_file);
				perror("Can't open output file");
				return EX_USAGE;
			}
		}
	}
	argc -= optind;
	argv += optind;

#ifdef HAVE_PLEDGE
	if (out == 1)
		pledge("stdio rpath inet dns", "");
	else
		pledge("stdio rpath wpath inet dns", "");
#endif

	if (argc != 2 && !check_only) {
		usage(prgname);
		return EX_USAGE;
	}

	/* parse config */
	struct config *c = parse_config(config_file);
	if (c == nil)
		return EX_UNAVAILABLE;

	if (print_config)
		for (size_t i = 0; i < c->len; ++i) {
			fprintf(stderr, "site: %s\nselector:\n", c->sources[i].name);
			struct selector *sel = c->sources[i].css_selector;
			while (sel != nil) {
				fprintf(stderr, "\t<%s id=%s class=%s />\n", sel->tag, sel->id, sel->class);
				sel = sel->next;
			}
			fprintf(stderr, "\n");
		}


	if (check_only) {
		printf("configuration OK\n");
		destroy_config(c);
		return 0;
	}

	bool res = fetch_lyrics(c, out, argv[0], argv[1]);

	destroy_config(c);

	if (!res) {
		fprintf(stderr, "Lyric not found.\n");
		return 1;
	}

	return 0;
}
