FLYR(1) - General Commands Manual

# NAME

**flyr** - fetch lyrics

# SYNOPSYS

**flyr**
\[**-hnp**]
\[**-f**&nbsp;*config*]
\[**-o**&nbsp;*out*]
\[artist]
\[song&nbsp;title]

# DESCRIPTION

The
**flyr**
utility retreive lyrics from the sites defined in
flyrrc(5)
and print the text on the screen.

The options are as follows:

**-f** *config*

> Specify the config file. Defaults to
> *~/.flyrrc*.

**-h**

> print a small usage

**-n**

> Configtest mode. Only check the configuration file for validity.

**-o**

> Specify the output file where the lyrics will be written. The file
> will be created if it not exists and will be overwritten if it exitst.

**-p**

> Print the config to stderr.

# SEE ALSO

flyrrc(5)

OpenBSD 6.3 - August 17, 2018
